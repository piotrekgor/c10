# **c10dataset.py**
---
# Contains dataset-related constants and helper functions. 
# Not to be changed.

## Constants 
    IMAGE_WIDTH = 32  
*width of single dataset image (in pixels)*

    IMAGE_HEIGHT = 32 
*height of single dataset image (in pixels)*

    IMAGE_DEPTH = 3
*number of channels of single dataset image (RGB)*

    CLASSES_COUNT = 10 
*number of classes (labels) to assing images to*

    BATCH_SIZE = 10_000 
*number of images per batch (images in single file)*

    TRAIN_BATCHES_COUNT = 5 
*number of training batches (training files)*

    TEST_IMAGES_COUNT = BATCH_SIZE 
*number of test images*

    TRAIN_IMAGES_COUNT = TRAIN_BATCHES_COUNT * BATCH_SIZE
*number of train images*

    DATA_LOCATION = 'data'
*name of directory containing dataset*

    DATA_URL = 'https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz'
*dataset location*

---

## Public methods: 
    plot_images(images, cls_true, class_names, plot_images_count, cls_pred=None, smooth=True)

*Renders images.*

Arguments:

- **images**: 4-dimensional array of images.
- **cls_true**: 1-dimensional array of images' true classes.
- **class_names**: 1-dimensional array of labels names.
- **plot_images_count**: number of images to plot.
- **cls_pred**: 1-dimensional array of images' predicted classes (default=None).
- **smooth**: whether to use interpolation to smooth down pixelated images.

---

    load_test_data()

*Loads all the test data from the dataset.*

Returns:

- 4-dimensional array of images
- 1-dimensional array of images' true labels
- 1-hot-encoded array of images' true labels

---

    load_training_data()

*Loads all the training data from the dataset.*

Returns:

- 4-dimensional array of images
- 1-dimensional array of images' true labels
- 1-hot-encoded array of images' true labels

---

    load_class_names()

*Loads class names.*

Returns: 

- 1-dimensional array of images' labels names

---

    download_and_extract_if_needed()

*Downloads and extracts dataset from web, unless found on the machine.*

# **c10main.py**
---
# Constants
# Changing these will result in different prediction accuracy.

    IMAGE_HEIGHT_CROPPED = 24
*width of cropped image (in pixels)*

    IMAGE_WIDTH_CROPPED = 24
*height of cropped image (in pixels)*

    PLOT_IMAGES_COUNT
*number of images to plot*

    DATA_TARGET_DIRECTORY = 'data'
*directory to get the dataset from*

    CHECKPOINT_DIRECTORY = 'checkpoints'
*directory to save checkpoints to*

    TRAINING_EPOCHS_COUNT
*number of training epochs*

    BATCH_SIZE
*size of a batch (number of images trained in a single epoch)*

    CHECKPOINT_STEP_PRINT
*training batch accuracy is printed after each number of epochs specified by this parameter*

    CHECKPOINT_STEP_SAVE
*model is saved as checkpoint after each number of epochs specified by this parameter*

    DISTORT_IMAGES = True
*indicates whether images should be distorted or not*

    CLEAR_CHECKPOINTS = False
*indicates whether checkpoints should be cleared before launching new training session*

    BATCH_SIZE_WHEN_NOT_DISTORTED
*size of a batch when DISTORT_IMAGES set to False*

---

## Public methods:

    pre_process(images, training)

*Preprocesses images, i.e. applies random distortions, cropping etc to images.*

Arguments:

- **x**: a tf.Tensor - placeholder containing images. A handle for feeding value, but not evaluated directly.
- **training**: indicates whether distorted images will be used for training or testing. If this parameter is set to True, distortions will include random cropping to a size specified in IMAGE_HEIGHT_CROPPED, IMAGE_WIDTH_CROPPED parameters, horizontal flipping, HUE adjustment, contrast, saturation and brightness changes. Otherwise images will only be centrally cropped.

---

    cnn(images, training) (cnn_2, cnn_3, cnn_4 contain other network models).

*Returns a convolutional neural network.*

Arguments:

- **images**: a tensor containing preprocessed images
- **training**: indicates whether CNN is created to train or test the model

Returns:

- a tensor of images' predicted classes
- the loss function

---

    create_network(training)

*Creates a convolutional neurol network using on of the functions described above.*

Arguments:

- **training**: indicates whether CNN is created to train or test the model

Returns:

- a tensor of images' predicted classes
- the loss function

---

    get_weights_variable(layer_name)

*Gets weights of specified layer name. Needed because of saver issues and because prettytensor doesn't allow to get inputs immiediately.*

Arguments:

- **layer_name**: layer name

Returns:

- a tensor of layer's weights

---

    get_layer_output(layer_name)

*Gets layer output similarily to the get_weights_variable function.*

Arguments:

- **layer_name**: layer name

Returns:

- a tensor of layer's outputs

---

    random_batch()

*Creates a random batch of size specified by TRAINING_BATCH_SIZE parameter.*

Returns:

- a tensor containing images
- a tensor containing true labels for the images in 1st parameter.

---

    optimize(num_iterations)

*Performs model optimization. Also prints optimization status and saves session.*

Arguments:

- **num_iterations**: number of epochs to perform optimization

---

    plot_example_errors(cls_pred, correct)

*Plots examples of wrong classified images.*

Arguments:

- **cls_pred**: a tensor with images' classes predicted by the cnn
- **correct**: a boolean array of size of TEST_IMAGE_COUNT, where True indicates a correct prediction

---

    plot_confusion_matrix(cls_pred)

*Prints confusion matrix.*

Arguments:

- **cls_pred**: array of classes predicted by the cnn

---

    plot_distorted_image(image, cls_true)

*Plots a distorted image to present image distortions.*

Arguments:

- **image**: image to be plotted (a 3-dim array)
- **cls_true**: a true class of an image

---

    get_test_image(i)

*Gets test image (image from the test batch) of specified index.*

Arguments:

- **i**: index of test image

Returns:

- test image of the index specified in the function parameter

---

    print_test_accuracy(show_example_errors=False, show_confusion_matrix=False)

*Prints all test results i.e. accuracy, example errors and confusion matrix.*

Arguments:

- **show_example_errors**: True if example errors (images) are to be plotted.
- **show_confusion_matrix**: True if confusion matrix is to be shown.




