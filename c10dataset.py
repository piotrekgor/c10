"""
Helper module for dataset operations
"""
import os
import urllib.request
import tarfile
import pickle
import numpy as np
import math
import matplotlib.pyplot as plt
"""
Constants defining Cifar-10 dataset
"""
IMAGE_WIDTH = 32
IMAGE_HEIGHT = 32
IMAGE_DEPTH = 3

CLASSES_COUNT = 10

BATCH_SIZE = 10_000
TRAIN_BATCHES_COUNT = 5

TEST_IMAGES_COUNT = BATCH_SIZE
TRAIN_IMAGES_COUNT = TRAIN_BATCHES_COUNT * BATCH_SIZE

DATA_LOCATION = 'data'
DATA_URL = 'https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz'
EXTRACTED_DIRECTORY_NAME = 'cifar-10-batches-py'

# Private methods

def _one_hot_encoded(classes, classes_count=None):
    """
    Returns a one hot encoded
    A 2 dimensional array of size [Number of images] x [CLASSES_COUNT] where every vector in 2nd dimension
    is one hot encoded - has all 0 except 1 index with 1 value
    :param classes:
    :param classes_count:
    :return:
    """
    if classes_count is None:
        classes_count = np.max(classes) + 1

    return np.eye(classes_count, dtype=float)[classes]


def _get_file_path(filename=""):
    """
    Returns full path to file from Cifar-10 dataset
    :param filename: Filename
    :return: Full path to file from Cifar-10 dataset
    """
    return os.path.join(DATA_LOCATION, EXTRACTED_DIRECTORY_NAME, filename)


def _unpickle(filename):
    """
    Gets data from file
    :param filename: gets raw data from file
    :return:
    """

    # Get file full path
    file_path = _get_file_path(filename)

    print("Loading data: " + file_path)

    with open(file_path, mode='rb') as file:
        # In Python 3.X it is important to set the encoding,
        # otherwise an exception is raised here.
        data = pickle.load(file, encoding='bytes')

    return data


def _load_data(filename):
    """
    Loads all images and labels from file
    Also converts RBG channels from range 0-255 to 0.0 - 1.0
    :param filename: Name of file
    :return: images, classes
    """

    # Load the pickled data-file.
    data = _unpickle(filename)

    # Get the raw images.
    raw_images = data[b'data']

    # Get the class-numbers for each image. Convert to numpy-array.
    cls = np.array(data[b'labels'])

    # Convert the images.
    # Convert the raw images from the data-files to floating-points.
    raw_float = np.array(raw_images, dtype=float) / 255.0

    # Reshape the array to 4-dimensions.
    images = raw_float.reshape([-1, IMAGE_DEPTH, IMAGE_HEIGHT, IMAGE_WIDTH])

    # Reorder the indices of the array.
    images = images.transpose([0, 2, 3, 1])

    return images, cls

# Public methods


def download_and_extract_if_needed():
    """
    Gets dataset from web if not found on the machine.
    """
    # Get filename from download URL
    filename = DATA_URL.split('/')[-1]
    file_path = os.path.join(DATA_LOCATION, filename)
    if os.path.exists(file_path):
        print('Data has already been downloaded to: %s' % file_path)
        return
    else:
        if not os.path.exists(DATA_LOCATION):
            os.makedirs(DATA_LOCATION)
        print('No dataset found in: %s\nDownloading...' % DATA_LOCATION)
        file_path, _ = urllib.request.urlretrieve(url=DATA_URL,
                                                  filename=file_path)
        print('Downloading finished to directory: %s\nExtracting...' % file_path)
        tarfile.open(name=file_path, mode="r:gz").extractall(DATA_LOCATION)
        print('Data successfully downloaded and extracted.')


def load_class_names():
    # Load the class-names from the pickled file.
    raw = _unpickle(filename="batches.meta")[b'label_names']
    # Convert from binary strings.
    names = [x.decode('utf-8') for x in raw]
    return names


def load_training_data():
    """
    Load all the training-data for the CIFAR-10 data-set.

    The data-set is split into 5 data-files which are merged here.

    Returns the images, class-numbers and one-hot encoded class-labels.
    """

    # Pre-allocate the arrays for the images and class-numbers for efficiency.
    images = np.zeros(shape=[TRAIN_IMAGES_COUNT, IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_DEPTH], dtype=float)
    cls = np.zeros(shape=[TRAIN_IMAGES_COUNT], dtype=int)

    # Begin-index for the current batch.
    begin = 0

    # For each data-file.
    for i in range(TRAIN_BATCHES_COUNT):
        # Load the images and class-numbers from the data-file.
        images_batch, cls_batch = _load_data(filename="data_batch_" + str(i + 1))

        # Number of images in this batch.
        num_images = len(images_batch)

        # End-index for the current batch.
        end = begin + num_images

        # Store the images into the array.
        images[begin:end, :] = images_batch

        # Store the class-numbers into the array.
        cls[begin:end] = cls_batch

        # The begin-index for the next batch is the current end-index.
        begin = end

    return images, cls, _one_hot_encoded(classes=cls, classes_count=CLASSES_COUNT)


def load_test_data():
    """
    Load all the test-data for the CIFAR-10 data-set.

    Returns the images, class-numbers and one-hot encoded class-labels.
    """

    images, cls = _load_data(filename="test_batch")

    return images, cls, _one_hot_encoded(classes=cls, classes_count=CLASSES_COUNT)


def plot_images(images, cls_true, class_names, plot_images_count, cls_pred=None, smooth=True):
    assert len(images) == len(cls_true) == plot_images_count
    # Create figure with sub-plots.
    size = int(math.sqrt(plot_images_count))
    fig, axes = plt.subplots(size, size)

    # Adjust vertical spacing if we need to print ensemble and best-net.
    if cls_pred is None:
        hspace = 0.3
    else:
        hspace = 0.6
    fig.subplots_adjust(hspace=hspace, wspace=0.3)

    for i, ax in enumerate(axes.flat):
        # Interpolation type.
        if smooth:
            interpolation = 'spline16'
        else:
            interpolation = 'nearest'

        # Plot image.
        ax.imshow(images[i, :, :, :],
                  interpolation=interpolation)

        # Name of the true class.
        cls_true_name = class_names[cls_true[i]]

        # Show true and predicted classes.
        if cls_pred is None:
            xlabel = "True: {0}".format(cls_true_name)
        else:
            # Name of the predicted class.
            cls_pred_name = class_names[cls_pred[i]]

            xlabel = "True: {0}\nPred: {1}".format(cls_true_name, cls_pred_name)

        # Show the classes as the label on the x-axis.
        ax.set_xlabel(xlabel)

        # Remove ticks from the plot.
        ax.set_xticks([])
        ax.set_yticks([])

    # Ensure the plot is shown correctly with multiple plots
    # in a single Notebook cell.
    plt.show()






