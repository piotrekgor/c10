import random

import c10dataset as ds
import tensorflow as tf
import numpy as np

import shutil
from sklearn.metrics import confusion_matrix
import time
from datetime import timedelta
import os
import prettytensor as pt

"""
CONSTANTS
change these to get different result
"""

IMAGE_HEIGHT_CROPPED = 24
IMAGE_WIDTH_CROPPED = 24

PLOT_IMAGES_COUNT = 16

DATA_TARGET_DIRECTORY = 'data'
CHECKPOINT_DIRECTORY = 'checkpoints'

TRAINING_EPOCHS_COUNT = 15_000
BATCH_SIZE = 64

CHECKPOINT_STEP_PRINT = 500
CHECKPOINT_STEP_SAVE = 1000

DISTORT_IMAGES = True
CLEAR_CHECKPOINTS = False
BATCH_SIZE_WHEN_NOT_DISTORTED = 256


if not DISTORT_IMAGES:
    TRAINING_EPOCHS_COUNT = int(ds.TRAIN_IMAGES_COUNT / BATCH_SIZE_WHEN_NOT_DISTORTED)


def _pre_process_image(image, is_training):
    """

    :param image:
    :param is_training:
    :return:
    """
    if is_training:
        """
        In case of training, distort image
        """
        # Randomly crop image
        image = tf.random_crop(image, size=[IMAGE_HEIGHT_CROPPED, IMAGE_WIDTH_CROPPED, ds.IMAGE_DEPTH])

        # Randomly flip image horizontally
        image = tf.image.random_flip_left_right(image)

        # Randomly adjust hue
        # max_delta must be in range [0:0.5]
        image = tf.image.random_hue(image, max_delta=0.05)

        # Randomly change contrast
        image = tf.image.random_contrast(image, lower=0.3,upper=1.0)

        # Randomly change brightness
        image = tf.image.random_brightness(image, max_delta=0.2)

        # Randomly change saturation
        image = tf.image.adjust_saturation(image, saturation_factor=1.1)

        # Some of the operations may cause overflowing (resulting in higher value than 1)
        # To prevent that behaviour limit the values of image pixels
        image = tf.minimum(image, 1.0)
        image = tf.maximum(image, 0.0)
    else:
        """
        In case of testing, do not distort image, only crop the input to match the size using while training.
        But use cropping around the centre instead of random cropping
        """
        image = tf.image.resize_image_with_crop_or_pad(image, IMAGE_HEIGHT_CROPPED, IMAGE_WIDTH_CROPPED)

    return image


def pre_process(images, training):
    return tf.map_fn(lambda image: _pre_process_image(image, training), images)


ds.download_and_extract_if_needed()

# Get labels
class_names = ds.load_class_names()
# Get train images, classes, labels
# labels_train is one hot encoded
images_train, classes_train, labels_train = ds.load_training_data()
# Get test images, classes, labels
# labels_test is one hot encoded
images_test, classes_test, labels_test = ds.load_test_data()

# random image index to display testing photos
random_image_index = random.randint(0, ds.TEST_IMAGES_COUNT - PLOT_IMAGES_COUNT)
# images to display (plot)
images = images_test[random_image_index:random_image_index+PLOT_IMAGES_COUNT]
# true classes of selected images
cls_true = classes_test[random_image_index:random_image_index+PLOT_IMAGES_COUNT]

# Plotting images to illustrate how input pictures look like
#ds.plot_images(images=images,
#               cls_true=cls_true,
#               class_names=class_names,
#               plot_images_count=PLOT_IMAGES_COUNT,
#               smooth=True)

# Placeholder for input parameters (bytes of image)
x = tf.placeholder(tf.float32,
                   shape=[None, ds.IMAGE_HEIGHT, ds.IMAGE_WIDTH, ds.IMAGE_DEPTH], name='input_placeholder')

# Placeholder for 'true labels' - correct classification
y_true = tf.placeholder(tf.float32,
                        shape=[None, ds.CLASSES_COUNT], name="classification_placeholder")
# True classes of input images
# dimension is deprecated, it is recommended to use axis
y_true_cls = tf.argmax(y_true, axis=1)


# Inputs for training
# only for displaying the distorted images
distorted_images = pre_process(images=x, training=True)

# 5346
def cnn(images, training):
    """
    Returns a cnn
    for 5_000/32
    """
    # Wrapping the input images as Pretty Tensor object to simplify modeling phase
    input_pretty = pt.wrap(images)

    if training:
        phase = pt.Phase.train
    else:
        phase = pt.Phase.infer

    # CNN model
    with pt.defaults_scope(activation_fn=tf.nn.relu, phase=phase):
        class_predicted, loss = input_pretty.\
            conv2d(kernel=5, depth=64, name='conv_layer1', batch_normalize=True).\
            max_pool(kernel=2, stride=2).\
            conv2d(kernel=2, depth=64, name='conv_layer2', batch_normalize=True).\
            max_pool(kernel=2, stride=2).\
            flatten().\
            fully_connected(size=256, name='dense1').\
            fully_connected(size=128, name='dense2'). \
            softmax_classifier(num_classes=ds.CLASSES_COUNT, labels=y_true)
    # softmax classifier is changing fully conected layer outputs into probability
    # it is used to illustrate the classification
    # also we obtain the loss function which we can minimize
    return class_predicted, loss
# 5614
def cnn_2(images, training):
    """
    Returns a cnn
    :param images:
    :param training:
    :return:
    """
    # Wrapping the input images as Pretty Tensor object to simplify modeling phase
    input_pretty = pt.wrap(images)

    if training:
        phase = pt.Phase.train
    else:
        phase = pt.Phase.infer

    # CNN model
    with pt.defaults_scope(activation_fn=tf.nn.relu, phase=phase):
        class_predicted, loss = input_pretty.\
            conv2d(kernel=5, depth=64, name='conv_layer1', batch_normalize=True).\
            max_pool(kernel=2, stride=2).\
            conv2d(kernel=5, depth=192, name='conv_layer2', batch_normalize=True). \
            conv2d(kernel=1, depth=192, name='conv_layer3', batch_normalize=True).\
            flatten().\
            fully_connected(size=256, name='dense1').\
            fully_connected(size=128, name='dense2'). \
            softmax_classifier(num_classes=ds.CLASSES_COUNT, labels=y_true)
    # softmax classifier is changing fully conected layer outputs into probability
    # it is used to illustrate the classification
    # also we obtain the loss function which we can minimize
    return class_predicted, loss
# 5700 without batch normalization
# time
def cnn_3(images, training):
    """
    Returns a cnn
    :param images:
    :param training:
    :return:
    """
    # Wrapping the input images as Pretty Tensor object to simplify modeling phase
    input_pretty = pt.wrap(images)

    if training:
        phase = pt.Phase.train
    else:
        phase = pt.Phase.infer

    # CNN model
    with pt.defaults_scope(activation_fn=tf.nn.relu, phase=phase):
        class_predicted, loss = input_pretty.\
            conv2d(kernel=3, depth=96, name='conv_layer1', batch_normalize=True).\
            conv2d(kernel=3, depth=96, name='conv_layer2', batch_normalize=True).\
            max_pool(kernel=3, stride=2).\
            conv2d(kernel=3, depth=192, name='conv_layer3', batch_normalize=True).\
            conv2d(kernel=3, depth=192, name='conv_layer4', batch_normalize=True).\
            max_pool(kernel=3, stride=2).\
            conv2d(kernel=3, depth=192, name='conv_layer5', batch_normalize=True).\
            conv2d(kernel=1, depth=192, name='conv_layer6', batch_normalize=True).\
            conv2d(kernel=1, depth=10, name='conv_layer7', batch_normalize=True).\
            flatten(). \
            fully_connected(size=128, name='dense2').\
            softmax_classifier(num_classes=ds.CLASSES_COUNT, labels=y_true)
    # softmax classifier is changing fully conected layer outputs into probability
    # it is used to illustrate the classification
    # also we obtain the loss function which we can minimize
    return class_predicted, loss
# 5460
def cnn_4(images, training):
    """
    Returns a cnn
    :param images:
    :param training:
    :return:
    """
    # Wrapping the input images as Pretty Tensor object to simplify modeling phase
    input_pretty = pt.wrap(images)

    if training:
        phase = pt.Phase.train
    else:
        phase = pt.Phase.infer

    # CNN model
    with pt.defaults_scope(activation_fn=tf.nn.relu, phase=phase):
        class_predicted, loss = input_pretty.\
            conv2d(kernel=5, depth=64, name='conv_layer1').\
            max_pool(kernel=2, stride=2).\
            batch_normalize().\
            conv2d(kernel=5, depth=64, name='conv_layer2').\
            batch_normalize().\
            max_pool(kernel=3, stride=2).\
            flatten().\
            fully_connected(size=256, name='dense1').\
            fully_connected(size=128, name='dense2'). \
            softmax_classifier(num_classes=ds.CLASSES_COUNT, labels=y_true)
    # softmax classifier is changing fully conected layer outputs into probability
    # it is used to illustrate the classification
    # also we obtain the loss function which we can minimize
    return class_predicted, loss

def create_network(training):
    """
    :param training: true if training network, false if test networks
    :return:
    """
    with tf.variable_scope('network', reuse=not training):
        # images are tensorflow placeholder
        images = x
        # preporcessing images, random dissortions
        if(DISTORT_IMAGES):
            images = pre_process(images=images, training=training)
            # create actual cnn
        class_predicted, loss = cnn_3(images=images, training=training)
    return class_predicted, loss

# TRAINING


# Tf variable updated each batch iteration
global_step = tf.Variable(initial_value=0,
                          name='global_step', trainable=False)

# we only need to minimize the loss function, so we can ignore the output
_, loss = create_network(training=True)
# setting the optimization function - in this case AdamOptimizer has been picked
# it is an extension to stochastic gradient descent that has been proven to work better and is recommended to use
optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(loss, global_step=global_step)


# TESTING


# Create network for testing
# in that case the loss function is not needed since it will not be optimized
y_pred, _ = create_network(training=False)
# dimension is deprecated, better use axis here
y_pred_cls = tf.argmax(y_pred, axis=1)
# compare results with true classes
correct_predition = tf.equal(y_pred_cls, y_true_cls)
# show accuracy as percentage
accuracy = tf.reduce_mean(tf.cast(correct_predition, tf.float32))

# saver object for saving session results
saver = tf.train.Saver()

###
# Needed because pretty tensor doesn't allow to get outputs easily
def get_weights_variable(layer_name):
    """

    :param layer_name:
    :return:
    """
    with tf.variable_scope('network/' + layer_name, reuse=True):
        variable = tf.get_variable('weights')

    return variable


def get_layer_output(layer_name):
    """

    :param layer_name:
    :return:
    """
    tensor_name = 'network/' + layer_name + '/Relu:0'
    tensor = tf.get_default_graph().get_tensor_by_name(tensor_name)

    return tensor


weights_conv1 = get_weights_variable(layer_name='conv_layer1')
weights_conv2 = get_weights_variable(layer_name='conv_layer2')
weights_conv3 = get_weights_variable(layer_name='conv_layer3')
weights_conv4 = get_weights_variable(layer_name='conv_layer4')
weights_conv5 = get_weights_variable(layer_name='conv_layer5')
weights_conv6 = get_weights_variable(layer_name='conv_layer6')
weights_conv7 = get_weights_variable(layer_name='conv_layer7')

output_conv1 = get_layer_output(layer_name='conv_layer1')
output_conv2 = get_layer_output(layer_name='conv_layer2')
output_conv3 = get_layer_output(layer_name='conv_layer3')
output_conv4 = get_layer_output(layer_name='conv_layer4')
output_conv5 = get_layer_output(layer_name='conv_layer5')
output_conv6 = get_layer_output(layer_name='conv_layer6')
output_conv7 = get_layer_output(layer_name='conv_layer7')
#
###

# Tensorflow session
session = tf.Session()

# If re-learn
# clear checkpoints
if CLEAR_CHECKPOINTS:
    shutil.rmtree(CHECKPOINT_DIRECTORY)

# Create directory for checkpoints
if not os.path.exists(CHECKPOINT_DIRECTORY):
    os.makedirs(CHECKPOINT_DIRECTORY)

save_path = os.path.join(CHECKPOINT_DIRECTORY, 'cifar10_cnn')

# First try to restore session weights and biases before
# re-learning network
try:
    print("Trying to restore last checkpoint ...")

    # Use TensorFlow to find the latest checkpoint - if any.
    last_chk_path = tf.train.latest_checkpoint(checkpoint_dir=CHECKPOINT_DIRECTORY)

    # Already passed epochs (data from checkpoint files)
    epochs_passed = int(last_chk_path.split('-')[-1])
    global_step.assign(epochs_passed)
    TRAINING_EPOCHS_COUNT -= epochs_passed

    # Try and load the data in the checkpoint.
    saver.restore(session, save_path=last_chk_path)

    # If we get to this point, the checkpoint was successfully loaded.
    print("Restored checkpoint from:", last_chk_path)
except Exception as e:
    # If the above failed for some reason, simply
    # initialize all the variables for the TensorFlow graph.
    print("Failed to restore checkpoint. Initializing variables instead.")
    session.run(tf.global_variables_initializer())


def random_batch():
    """
    Get batch (images, labels) of TRAINING_BATCH_SIZE
    :return: Batch (images, labels) of TRAINING_BATCH_SIZE
    """
    num_images = len(images_train)

    idx = np.random.choice(num_images,
                           size=BATCH_SIZE,
                           replace=False)

    x_batch = images_train[idx, :, :, :]
    y_batch = labels_train[idx, :]

    return x_batch, y_batch


def optimize(num_iterations):
    """
    Performs optimization
    :param num_iterations: number of epochs
    :return:
    """
    # Start-time used for printing time-usage below.
    start_time = time.time()

    for i in range(num_iterations):
        # Get batch (images, labels)
        if DISTORT_IMAGES:
            x_batch, y_true_batch = random_batch()
        else:
            start_index = i * BATCH_SIZE_WHEN_NOT_DISTORTED
            end_index = start_index + BATCH_SIZE_WHEN_NOT_DISTORTED
            x_batch = images_train[start_index:end_index, :, :, :]
            y_true_batch = labels_train[start_index:end_index, :]


        # Tensorflow needs a dictionary
        feed_dict_train = {x: x_batch,
                           y_true: y_true_batch}

        # !!
        # Run the optimizer using this batch of training data.
        # TensorFlow assigns the variables in feed_dict_train
        # to the placeholder variables and then runs the optimizer.
        # We also want to retrieve the global_step counter.
        i_global, _ = session.run([global_step, optimizer],
                                  feed_dict=feed_dict_train)

        # Print status to give feedback from cnn every CHECKPOINT_STEP_PRINT
        if (i_global % CHECKPOINT_STEP_PRINT == 0) or (i == num_iterations - 1):
            # Calculate the accuracy on the training-batch.
            batch_acc = session.run(accuracy,
                                    feed_dict=feed_dict_train)

            # Print status.
            msg = "Global Step: {0:>6}, Training Batch Accuracy: {1:>6.1%}"
            print(msg.format(i_global, batch_acc))

        # Save a checkpoint to disk every 1000 iterations (and last).
        if (i_global % CHECKPOINT_STEP_SAVE == 0) or (i == num_iterations - 1):
            # Save all variables of the TensorFlow graph to a
            # checkpoint. Append the global_step counter
            # to the filename so we save the last several checkpoints.
            saver.save(session,
                       save_path=save_path,
                       global_step=global_step)

            print("Saved checkpoint.")

    # Ending time.
    end_time = time.time()

    # Difference between start and end-times.
    time_dif = end_time - start_time

    # Print the time-usage.
    print("Time usage: " + str(timedelta(seconds=int(round(time_dif)))))


def plot_example_errors(cls_pred, correct):
    """
    Plots some of the wrong classified images
    :param cls_pred: An array with predicted classes for each image
    :param correct: A boolean array of size of TEST_IMAGE_COUNT where True indicates a correct predition
    :return:
    """

    # Negate array
    incorrect = (correct == False)

    # Get the images from the test-set that have been
    # incorrectly classified.
    images = images_test[incorrect]
    # Get predicted classes for each image
    cls_pred = cls_pred[incorrect]
    # Get true class for each image
    cls_true = classes_test[incorrect]

    # Plot the first images.
    ds.plot_images(images=images[0:PLOT_IMAGES_COUNT],
                   cls_true=cls_true[0:PLOT_IMAGES_COUNT],
                   cls_pred=cls_pred[0:PLOT_IMAGES_COUNT],
                   class_names=class_names,
                   plot_images_count=PLOT_IMAGES_COUNT)


def plot_confusion_matrix(cls_pred):
    """
    Prints confusion matrix using 3rd-party library
    :param cls_pred:
    :return:
    """
    cm = confusion_matrix(y_true=classes_test,  # True class for test-set.
                          y_pred=cls_pred)  # Predicted class.

    # Print the confusion matrix as text.
    for i in range(ds.CLASSES_COUNT):
        # Append the class-name to each line.
        class_name = "({}) {}".format(i, class_names[i])
        print(cm[i, :], class_name)

    # Print the class-numbers for easy reference.
    class_numbers = [" ({0})".format(i) for i in range(ds.CLASSES_COUNT)]
    print("".join(class_numbers))


def _predict_cls(images, labels, cls_true):
    """
    Predict images using optimized CNN
    :param images: Images to oredict
    :param labels: One hot encoded
    :param cls_true: True image classes
    :return:
    """
    # Get number of images
    num_images = len(images)

    # Allocate array for predicted classes
    cls_pred = np.zeros(shape=num_images, dtype=np.int)

    i = 0
    while i < num_images:
        # Get upper index
        j = min(i + BATCH_SIZE, num_images)

        # Feed dictionary needed by Tensorflow
        feed_dict = {x: images[i:j, :],
                     y_true: labels[i:j, :]}

        # Calculate prediction
        cls_pred[i:j] = session.run(y_pred_cls, feed_dict=feed_dict)

        # Move index to fill next batch of classes
        i = j

    # Create a boolean array
    correct = (cls_true == cls_pred)

    return correct, cls_pred


def plot_distorted_image(image, cls_true):
    """
    Plots a distorted image
    :param image: Image to be plotted
    :param cls_true: true class of image
    :return:
    """

    # Prepare N copies of image
    image_duplicates = np.repeat(image[np.newaxis, :, :, :], PLOT_IMAGES_COUNT, axis=0)

    # Create dictionary for tensorflow session
    feed_dict = {x: image_duplicates}

    # Run the session to distort image
    result = session.run(distorted_images, feed_dict=feed_dict)

    # Plot the images.
    ds.plot_images(images=result,
                   cls_true=np.repeat(cls_true, PLOT_IMAGES_COUNT),
                   class_names=class_names,
                   plot_images_count=PLOT_IMAGES_COUNT)


def get_test_image(i):
    """
    Gets test image of specified index
    :param i:
    :return:
    """
    return images_test[i, :, :, :], classes_test[i]


#img, cls = get_test_image(random_image_index)

#plot_distorted_image(img, cls)


def print_test_accuracy(show_example_errors=False,
                        show_confusion_matrix=False):
    """
    Prints all test results
    :param show_example_errors: True if example errors are to be seen
    :param show_confusion_matrix: True if confusion matrix is to be seen
    :return:
    """
    # Get test results
    correct, cls_pred = _predict_cls(images=images_test,
                                     labels=labels_test,
                                     cls_true=classes_test)

    # Classification accuracy and the number of correct classifications.
    acc = correct.mean()
    correct_count = correct.sum()

    # Number of images being classified.
    num_images = len(correct)

    # Print the accuracy.
    msg = "Accuracy on Test-Set: {0:.1%} ({1} / {2})"
    print(msg.format(acc, correct_count, num_images))

    # Plot some examples of mis-classifications, if desired.
    if show_example_errors:
        print("Example errors:")
        plot_example_errors(cls_pred=cls_pred, correct=correct)

    # Plot the confusion matrix, if desired.
    if show_confusion_matrix:
        print("Confusion Matrix:")
        plot_confusion_matrix(cls_pred=cls_pred)


optimize(TRAINING_EPOCHS_COUNT)

print_test_accuracy(show_example_errors=True,
                    show_confusion_matrix=True)

# Close session
session.close()
